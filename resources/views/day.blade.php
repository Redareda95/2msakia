<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title> الامساكيه</title>
	<meta name="description" content="Ramadan prayers timing and Imsakya 2018">
	<meta name="author" content="GTSAW">

	<!-- Mobile Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Favicon -->
	<link rel="shortcut icon" href="/images/16.png">

	<!-- Web Fonts -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Raleway:700,400,300" rel="stylesheet" type="text/css">
	
		<link href="https://fonts.googleapis.com/css?family=Amiri" rel="stylesheet">
	<!-- Bootstrap core CSS -->
	<link href="/bootstrap/css/bootstrap.css" rel="stylesheet">

	<!-- Font Awesome CSS -->
	<link href="/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="https://i.icomoon.io/public/temp/e9468faa94/TestProject/style.css">
	<!-- Plugins -->
	<link href="/css/animations.css" rel="stylesheet">

	<!-- Worthy core CSS file -->
	<link rel="stylesheet"  href="/css/layout.css">	
	<link href="/css/color.css" rel="stylesheet">
</head>
<body class="no-trans sun sun1 "  style="overflow-x: hidden;" >
	<header>
		<nav id="nav-wrap">
			<div class="header-left clearfix">
				<!-- logo -->
				<div class="logo smooth-scroll ">
					<a href="/"><img id="logo" src="/images/500.png"></a>
				</div>
			</div>
      	</nav>   
	</header>
	<section style="margin-top: 200px ;" class="menu">
		<div class="container">		
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style=" height:400px;">
					<h2  class="heading">{{ $month->day_name}}</h2><br>
					<h2 class="heading" >{{ $month->melady_day }}</h2><br>
					<h2 class="heading" >{{ $month->day }} رمضان </h2><br>
				</div>
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"  style=" height: 500px;"> 
         <div id="weather-widget" class="lightning">    
			<section id="current-temp">
				@php
				date_default_timezone_set('Africa/Cairo');
				@endphp
			<div class="digital-clock temp">{{ date('h:i A') }} </div>
			</section>
			<section id="forecast" class="hidden-xs hidden-sm">
				<ul style="list">
					
				
				
					<li data-icon="rainy" data-color="rain-a">
						<span class="day1">العشاء</span>
						<span class="icon icon-rainy"></span>
						<span class="temp pray">{{ $month->esha }}  <span class="time1">مساء</span></span>
					</li>
						<li data-icon="lightning" data-color="lightning-a">
						<span class="day1">المغرب</span>
						<span class="icon icon-lightning"></span>
						<span class="temp pray ">{{ $month->maghreb }} <span class="time1">مساء</span></span>
					</li>
					
				
						<li data-icon="cloudy2" data-color="cloudy-b">
						<span class="day1">العصر</span>
						<span class="icon icon-cloudy2"></span>
						<span class="temp pray ">{{ $month->asr }}<span class="time1">مساء</span></span>
					</li>
						<li data-icon="cloudy" data-color="cloudy-c" >
						<span class="day1">الظهر</span>
						<span class="icon icon-cloudy"></span>
						<span class="temp pray">{{ $month->dohr }}<span class="time1">صباحا</span></span>
					</li>
						<li data-icon="sun" data-color="sun-b">
						<span class="day1">الشروق</span>
						<span class="icon icon-sun"></span>
						<span class="temp pray ">{{ $month->sherouk }}<span class="time1">صباحا</span></span>
					</li>
					<li  class="current"  data-icon="sun" data-color="sun-c"  style="margin-top:50px">
						<span class="day1">الفجر</span>
						<span class="icon icon-sun"></span>
						<span class="temp pray">{{ $month->fager }}<span class="time1">صباحا</span></span>
					</li>
				</ul>
			</section>
		
			<section id="forecast2" class="hidden-md hidden-lg">
				<ul style="list">
					
				
						<li data-icon="sun" data-color="sun-b1" style=">
						<span class="day1">الشروق</span>
						<span class="icon icon-sun"></span>
						<span class="temp pray ">{{ $month->sherouk }}<span class="time1">صباحا</span></span>
					</li>
					<li  class="current"  data-icon="sun" data-color="sun-c1"  >
						<span class="day1">الفجر</span>
						<span class="icon icon-sun"></span>
						<span class="temp pray">{{ $month->fager }}<span class="time1">صباحا</span></span>
					</li>
					<li data-icon="cloudy2" data-color="cloudy-b1">
						<span class="day1">العصر</span>
						<span class="icon icon-cloudy2"></span>
						<span class="temp pray ">{{ $month->asr }}<span class="time1">مساء</span></span>
					</li>
						<li data-icon="cloudy" data-color="cloudy-c1" >
						<span class="day1">الظهر</span>
						<span class="icon icon-cloudy"></span>
						<span class="temp pray">{{ $month->dohr }}<span class="time1">صباحا</span></span>
					</li>
					
					
					<li data-icon="rainy" data-color="rain-a1">
						<span class="day1">العشاء</span>
						<span class="icon icon-rainy"></span>
						<span class="temp pray">{{ $month->esha }}  <span class="time1">مساء</span></span>
					</li>
					
					<li data-icon="lightning" data-color="lightning-a1"  style="margin-top:50px">
						<span class="day1">المغرب</span>
						<span class="icon icon-lightning"></span>
						<span class="temp pray ">{{ $month->maghreb }} <span class="time1">مساء</span></span>
					</li>
				</ul>
			</section>
					</div>
				</div>
			</div>
		</div>
	</section>
		<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    	<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

		<!-- JavaScript files placed at the end of the document so the pages load faster
		================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="/plugins/modernizr.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="/plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Backstretch javascript -->
		<script type="text/javascript" src="/plugins/jquery.backstretch.min.js"></script>

		<!-- Appear javascript -->
		<script type="text/javascript" src="/plugins/jquery.appear.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="/js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="/js/custom.js"></script>
<script>
		(function($){
	//write our scripts here
  var fadespeed = 500;
	$("#forecast li").on("click" , function(){
    
    //set main variable
		var $this = $(this),
			main = $this.parents("#weather-widget");
      mainDate = main.find("header"),
			mainTemp = main.find("h1.temp"),
			mainHi = mainTemp.next().find("span.hi"),
			mainLo = mainTemp.next().find("span.lo"),
			details = mainTemp.next(),
			
      //get values and store them
			temp = $this.find(".temp").text(),
			lo = $this.data("lo"),
			hi = $this.data("hi"),
      date = $this.data("date");
			icon = $this.data("icon"),
			color = $(this).data("color");

		//click functions
		$this.addClass("current").siblings().removeClass("current");
    
      //set the values
      mainTemp.text(temp);
      mainDate.text(date);
      details.stop().fadeTo(fadespeed , 0 , function(){
        $(this).stop().fadeTo(fadespeed ,1)
        details.find("i").removeClass().addClass("icon-" + icon);
        mainHi.text(hi);
        mainLo.text(lo);
      });
      $("body").removeClass().addClass(color);
	  });
})(jQuery);
		</script>
		<script>
		(function($){
	//write our scripts here
  var fadespeed = 500;
	$("#forecast2 li").on("click" , function(){
    
    //set main variable
		var $this = $(this),
			main = $this.parents("#weather-widget");
      mainDate = main.find("header"),
			mainTemp = main.find("h1.temp"),
			mainHi = mainTemp.next().find("span.hi"),
			mainLo = mainTemp.next().find("span.lo"),
			details = mainTemp.next(),
			
      //get values and store them
			temp = $this.find(".temp").text(),
			lo = $this.data("lo"),
			hi = $this.data("hi"),
      date = $this.data("date");
			icon = $this.data("icon"),
			color = $(this).data("color");

		//click functions
		$this.addClass("current").siblings().removeClass("current");
    
      //set the values
      mainTemp.text(temp);
      mainDate.text(date);
      details.stop().fadeTo(fadespeed , 0 , function(){
        $(this).stop().fadeTo(fadespeed ,1)
        details.find("i").removeClass().addClass("icon-" + icon);
        mainHi.text(hi);
        mainLo.text(lo);
      });
      $("body").removeClass().addClass(color);
	  });
})(jQuery);
		</script>
<script>
		$(document).ready(function() {
  clockUpdate();
  setInterval(clockUpdate, 1000);
})

function clockUpdate() {
  var date = new Date();
  $('.digital-clock').css({'color': '#fff', 'text-shadow': '0 0 6px #ff0'});
  function addZero(x) {
    if (x < 10) {
      return x = '0' + x;
    } else {
      return x;
    }
  }

  function twelveHour(x) {
    if (x > 12) {
      return x = x - 12;
    } else if (x == 0) {
      return x = 12;
    } else {
      return x;
    }
  }

  var h = addZero(twelveHour(date.getHours()));
  var m = addZero(date.getMinutes());
  var s = addZero(date.getSeconds());

  $('.digital-clock').text(h + ':' + m + ':' + s)
}
		</script>
		
		
	</body>
</html>


