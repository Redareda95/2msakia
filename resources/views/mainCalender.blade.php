<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"><!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title>الامساكيه</title>
		<meta name="description" content="Ramadan prayers timing and Imsakya 2018">
		<meta name="author" content="GTSAW">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="/images/16.png">

		<!-- Web Fonts -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Raleway:700,400,300" rel="stylesheet" type="text/css">
		
   		<link href="https://fonts.googleapis.com/css?family=Amiri" rel="stylesheet">

		<!-- Bootstrap core CSS -->
		<link href="/bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
		<!-- Plugins -->
		<link href="/css/animations.css" rel="stylesheet">


		<link rel="stylesheet" type="text/css" href="css/layout.css">
		<link href="/css/color.css" rel="stylesheet">
		<style>
		.button {
  font-family: "Montserrat", "Trebuchet MS", Helvetica, sans-serif;
  -webkit-font-smoothing: antialiased;
  position: relative;
  padding: .8em 1.4em;
  padding-right: 4.7em;
  border: none;
  color: white;
  transition: .2s;
}

.button:before {
  background: rgba(0, 0, 0, 0.1);
}
.button:hover {
  background: #0079a5;
}
.button:active,
.button:focus {
  background: #002e3f;
  outline: none;
}
.button {
  min-width: 5em;
}
.arrow {
  background: #FE5F55;
}
.arrow:hover {
  background: #fe2f22;
}
.arrow:active,
.arrow:focus {
  background: #b90c01;
}
.arrow:hover:after {
  -webkit-animation: bounceright .3s alternate ease infinite;
  animation: bounceright .3s alternate ease infinite;
}
</style>
</head>

	<body class="no-trans " >
		<header>
		      <nav id="nav-wrap">
    
	<div class="header-left clearfix">

							<!-- logo -->
							<div class="logo smooth-scroll " >
								<a href="/"><img id="logo" src="/images/500.png" ></a>
							</div>
						</div>
    
      </nav> <!-- end #nav-wrap -->

	   
		</header>

<h1 > <i class="fa fa-moon-o"></i>امساكيه رمضان  </h1>
<br>
<br>


<div class="container">
 
 <div class="image-wrap"></div>
  <div class="row ">
    <div class="col-xs-12 col-sm-12" style="width: 100wh; height: 100vh; overflow-x: scroll; ">
 		<table  class="table table-responsive table-hover footable" style="text-align:center; direction: rtl;border: none">
	        <thead class="hidden-sm hidden-xs">
	          <tr style="background-color: #5070A4" >
	            <th style="text-align: center ; " class="day">اليوم</th>
	            <th   style="text-align: center;" data-hide="phone,tablet">رمضان</th>
	            <th  style="text-align: center" data-hide="phone">ميلادي</th>
	            <th   style="text-align: center">الفجر</th>
	            <th  style="text-align: center" data-hide="phone">الشروق</th>
	            <th  style="text-align: center">الظهر</th>
	            <th   style="text-align: center" data-hide="phone,tablet">العصر</th>
	            <th style="text-align: center" class="time" data-hide="phone">المغرب</th>
	            <th  style="text-align: center">العشاء</th>
	          </tr>
	        </thead>
	          <thead class="hidden-md  hidden-lg">
	          <tr style="background-color: #5070A4;text-algin:center" >
	            <th style="text-align: center ; padding-bottom: 20px " class="day">اليوم</th>
	            <th   style="text-align: center; padding-bottom: 20px" data-hide="phone,tablet">رمضان</th>
	            <th  style="text-align: center;     padding-bottom: 20px" data-hide="phone">ميلادي</th>
	             <th  style="text-align: center"data-hide="phone">مواعيد الصلاه</th>
	          </tr>
	        </thead>
	       	<tbody style="background-color: rgba(0,0,0,0.8);" class="hidden-sm hidden-xs">
	        @foreach($monthes as $month)
	          <tr>
	            <td class="day"><a href="/day/{{ $month->id }}">{{ $month->day_name }}</a></td>
	            <td> <a href="/day/{{ $month->id }}">{{ $month->day }}</a></td>
	            <td>{{ $month->melady_day}}</td>
	            <td >{{ $month->fager }}</td>
	            <td >{{ $month->sherouk }}</td>
	            <td >{{ $month->dohr }}</td>
	            <td >{{ $month->asr }}</td>
	            <td class="time" >{{ $month->maghreb }}</td>
	            <td >{{ $month->esha }}</td>
	          </tr>
	        @endforeach
			</tbody>
			
			
	<tbody style="background-color: rgba(0,0,0,0.8); text-algin:center" class="hidden-md  hidden-lg">
	        @foreach($monthes as $month)
	          <tr>
	            <td  style="text-align: center ; padding-top: 20px " class="day"><a href="/day/{{ $month->id }}">{{ $month->day_name }}</a></td>
	            <td  style="text-align: center ; padding-top: 20px "> <a href="/day/{{ $month->id }}">{{ $month->day }}</a></td>
	            <td style="text-align: center ; padding-top: 20px ">{{ $month->melady_day}}</td>
	           <td  class="button  arrow"><a href="/day/{{ $month->id }}"> مواعيد الصلاه</a></td>
	          </tr>
	        @endforeach
			</tbody>
		</table>
	</div>
   </div>
  </div>
    	<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    	<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
		<!-- Modernizr javascript -->
		<script type="text/javascript" src="/plugins/modernizr.js"></script>
		<!-- Isotope javascript -->
		<script type="text/javascript" src="/plugins/isotope/isotope.pkgd.min.js"></script>
		<!-- Backstretch javascript -->
		<script type="text/javascript" src="/plugins/jquery.backstretch.min.js"></script>
		<!-- Appear javascript -->
		<script type="text/javascript" src="/plugins/jquery.appear.js"></script>
		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="/js/template.js"></script>
		<!-- Custom Scripts -->
		<script type="text/javascript" src="/js/custom.js"></script>

<script>
$('.footable').footable({
  calculateWidthOverride: function() {
    return { width: $(window).width() };
  }
}); 		
</script>


</body>
</html>