@if (Session::has('message'))
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

                @if (Session::has('message'))

                    <div class="modal-header" style="background-color: mediumseagreen">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" style="color: white">Success</h4>
                    </div>
                    <div class="modal-body" style="color: green">
                        <p>{{ Session::get('message') }}</p>
                    </div>
            </div>
            @endif
            <div class="modal-footer" style="display: none">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
    </div>
@endif

@if ($errors->any())
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

                @if ($errors->any())
                    <div class="modal-header" style="background-color: #db0141; color: white">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" style="color: white">Errors found</h4>
                    </div>
                    <div class="modal-body" style="color: #db0141">
                        @foreach($errors->all() as $error)
                            <p>* {{ $error }}</p>
                        @endforeach

                    </div>

                @endif


                <div class="modal-footer" style="background-color: #db0141; color: white; display: none">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@endif
<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <a href="{{url('home')}}" class="logo"><span>GT<span>SAW</span></span><i class="mdi mdi-layers"></i></a>
    </div>

    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container-fluid">

            <!-- Page title -->
            <ul class="nav navbar-nav list-inline navbar-left">
                <li class="list-inline-item">
                    <button class="button-menu-mobile open-left">
                        <i class="mdi mdi-menu"></i>
                    </button>
                </li>
                <li class="list-inline-item">
                    <h4 class="page-title">Ramadan</h4>
                </li>
            </ul>
        </div><!-- end container -->
    </div><!-- end navbar -->
</div>