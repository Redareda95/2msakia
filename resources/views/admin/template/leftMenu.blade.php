<div class="sidebar-inner slimscrollleft">

    <!-- UserController -->
    <div class="user-box">
        <div class="user-img">
            <img src="{{ asset('admin/assets/images/users/avatar-1.jpg') }}" alt="user-img" title="Mat Helme" class="rounded-circle img-thumbnail img-responsive">
            <div class="user-status offline"><i class="mdi mdi-adjust"></i></div>
        </div>
        <h5><a>{{ auth()->user()->name }}</a> </h5>
        <ul class="list-inline">
            <li class="list-inline-item">
                <a href="#" >
                    <i class="mdi mdi-settings"></i>
                </a>
            </li>

            <li class="list-inline-item">
                <form action="logout" method="POST">
                    @csrf
                    <button class="text-custom">
                        <i class="mdi mdi-power"></i>
                    </button>
                </form>
            </li>
        </ul>
    </div>
    <!-- End UserController -->

    <!--- Sidemenu -->
    <div id="sidebar-menu">
        <ul>
            <li class="text-muted menu-title">Navigation</li>

            <li>
                <a href="{{ url('home') }}" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span> </a>
            </li>

            <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-format-font"></i><span> Day </span> </a>
                <ul class="list-unstyled">
                    <li><a href="{{ route('day.create') }}">Add day</a></li>
                    <li><a href="{{ route('day.index') }}">Show Days</a></li>
                </ul>
            </li>

            <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-view-list"></i> <span> Admin </span></a>
                <ul class="list-unstyled">
                    <li><a href="{{ route('user.create') }}">Add Admin</a></li>
                    <li><a href="{{ route('user.index') }}">Show Admins</a></li>
                </ul>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -->
    <div class="clearfix"></div>

</div>