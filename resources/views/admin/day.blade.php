<!DOCTYPE html>
<html>

<!-- Mirrored from coderthemes.com/adminto/bs4/light/form-advanced.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 11:28:18 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">
    <link rel="shortcut icon" href="{{ asset('admin/assets/images/favicon.ico') }}">
    <title>Admin Home Page</title>
    <!-- Plugins css-->
    <link href="{{ asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/assets/plugins/multiselect/css/multi-select.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/assets/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/assets/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/assets/plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

    <!-- App css -->
    <link href="{{ asset('admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/assets/css/style.css') }}" rel="stylesheet" type="text/css" />

    <script src="{{ asset('admin/assets/js/modernizr.min.js') }}"></script>

</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
@include('admin.template.topMenu')
<!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        @include('admin.template.leftMenu')

    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                @if(isset($add))
                    <div class="row" style="width: 50%;margin-left:25% ">
                    <div class="col-lg-12" >
                        <form action="{{ route('day.store') }}" method="post">
                            @csrf
                            <h1 class="text-center"> Add Day </h1>
                            <br>
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Enter the Day Number:</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input id="demo3" type="number" value="" name="day">
                                        <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Enter Day in Gregorian Year</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" name="melady_day" class="form-control" placeholder="mm/dd/yyyy" id="datepicker">
                                        <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Enter ElEmsak Time</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input id="timepicker" name="emsak" type="text" class="form-control">
                                        <span class="input-group-addon"><i class="mdi mdi-clock"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Enter Elfager Time</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input id="timepicker2" name="fager" type="text" class="form-control">
                                        <span class="input-group-addon"><i class="mdi mdi-clock"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Enter Elsherok Time</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input id="timepicker3" name="sherouk" type="text" class="form-control">
                                        <span class="input-group-addon"><i class="mdi mdi-clock"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Enter Eldohr Time</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input id="timepicker4" name="dohr" type="text" class="form-control">
                                        <span class="input-group-addon"><i class="mdi mdi-clock"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Enter Elasr Time</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input id="timepicker5" name="asr" type="text" class="form-control">
                                        <span class="input-group-addon"><i class="mdi mdi-clock"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Enter Elmaghreb Time</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input id="timepicker6" name="maghreb" type="text" class="form-control">
                                        <span class="input-group-addon"><i class="mdi mdi-clock"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Enter Elesha Time</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input id="timepicker7" name="esha" type="text" class="form-control">
                                        <span class="input-group-addon"><i class="mdi mdi-clock"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-3 col-sm-9 m-t-15">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end col-12 -->
                </div>
                @endif
                @if(isset($edit))
                        <div class="row" style="width: 50%;margin-left:25% ">
                            <div class="col-lg-12" >
                                <form action="{{ url( 'day/'.$edit->id ) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger waves-effect waves-light">
                                        Delete
                                    </button>
                                </form>
                                <form action="{{ route('day.update',$edit->id) }}" method="post">
                                    @csrf
                                    @method('PATCH')
                                    <h1 class="text-center"> Edit Day </h1>
                                    <br>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Enter the Day Number:</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input id="demo3" type="number"  name="day" value="{{ $edit->day }}">
                                                <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Enter Day in Gregorian Year</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="melady_day" class="form-control" value="{{ $edit->melady_day }}" placeholder="mm/dd/yyyy" id="datepicker">
                                                <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Enter ElEmsak Time</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input id="timepicker" name="emsak"value="{{ $edit->emsak }}"  type="text" class="form-control">
                                                <span class="input-group-addon"><i class="mdi mdi-clock"></i></span>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Enter Elfager Time</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input id="timepicker2" name="fager" value="{{ $edit->fager }}" type="text" class="form-control">
                                                <span class="input-group-addon"><i class="mdi mdi-clock"></i></span>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Enter Elsherok Time</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input id="timepicker3" name="sherouk" value="{{ $edit->sherouk }}" type="text" class="form-control">
                                                <span class="input-group-addon"><i class="mdi mdi-clock"></i></span>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Enter Eldohr Time</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input id="timepicker4" name="dohr" value="{{ $edit->dohr }}" type="text" class="form-control">
                                                <span class="input-group-addon"><i class="mdi mdi-clock"></i></span>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Enter Elasr Time</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input id="timepicker5" name="asr" value="{{ $edit->asr }}" type="text" class="form-control">
                                                <span class="input-group-addon"><i class="mdi mdi-clock"></i></span>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Enter Elmaghreb Time</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input id="timepicker6" name="maghreb" value="{{ $edit->maghreb }}" type="text" class="form-control">
                                                <span class="input-group-addon"><i class="mdi mdi-clock"></i></span>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Enter Elesha Time</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input id="timepicker7" name="esha" value="{{ $edit->esha }}" type="text" class="form-control">
                                                <span class="input-group-addon"><i class="mdi mdi-clock"></i></span>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-3 col-sm-9 m-t-15">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                save
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- end col-12 -->
                        </div>
                    @endif
            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            2016 - 2018 © Adminto. Coderthemes.com
        </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <!-- Right Sidebar -->
    <!-- /Right-bar -->

</div>
<!-- END wrapper -->


<!-- jQuery  -->
<script src="{{ asset('admin/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/detect.js') }}"></script>
<script src="{{ asset('admin/assets/js/fastclick.js') }}"></script>
<script src="{{ asset('admin/assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('admin/assets/js/waves.js') }}"></script>
<script src="{{ asset('admin/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('admin/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('admin/assets/js/jquery.scrollTo.min.js') }}"></script>

<!-- Plugins Js -->
<script src="{{ asset('admin/assets/plugins/switchery/switchery.min.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/assets/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/assets/plugins/jquery-quicksearch/jquery.quicksearch.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/select2/dist/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/plugins/moment/moment.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>

<!-- App js -->
<script src="{{ asset('admin/assets/js/jquery.core.js') }}"></script>
<script src="{{ asset('admin/assets/js/jquery.app.js') }}"></script>

<script>
    jQuery(document).ready(function() {

        //advance multiselect start
        $('#my_multi_select3').multiSelect({
            selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            afterInit: function (ms) {
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function (e) {
                        if (e.which === 40) {
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function (e) {
                        if (e.which == 40) {
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function () {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function () {
                this.qs1.cache();
                this.qs2.cache();
            }
        });

        // Select2
        $(".select2").select2();

        $(".select2-limiting").select2({
            maximumSelectionLength: 2
        });

    });

    //Bootstrap-TouchSpin
    $(".vertical-spin").TouchSpin({
        verticalbuttons: true,
        buttondown_class: "btn btn-primary",
        buttonup_class: "btn btn-primary",
        verticalupclass: 'ti-plus',
        verticaldownclass: 'ti-minus'
    });
    var vspinTrue = $(".vertical-spin").TouchSpin({
        verticalbuttons: true
    });
    if (vspinTrue) {
        $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
    }

    $("input[name='demo1']").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        buttondown_class: "btn btn-primary",
        buttonup_class: "btn btn-primary",
        postfix: '%'
    });
    $("input[name='demo2']").TouchSpin({
        min: -1000000000,
        max: 1000000000,
        stepinterval: 50,
        buttondown_class: "btn btn-primary",
        buttonup_class: "btn btn-primary",
        maxboostedstep: 10000000,
        prefix: '$'
    });
    $("input[name='demo3']").TouchSpin({
        buttondown_class: "btn btn-primary",
        buttonup_class: "btn btn-primary"
    });
    $("input[name='demo3_21']").TouchSpin({
        initval: 40,
        buttondown_class: "btn btn-primary",
        buttonup_class: "btn btn-primary"
    });
    $("input[name='demo3_22']").TouchSpin({
        initval: 40,
        buttondown_class: "btn btn-primary",
        buttonup_class: "btn btn-primary"
    });

    $("input[name='demo5']").TouchSpin({
        prefix: "pre",
        postfix: "post",
        buttondown_class: "btn btn-primary",
        buttonup_class: "btn btn-primary"
    });
    $("input[name='demo0']").TouchSpin({
        buttondown_class: "btn btn-primary",
        buttonup_class: "btn btn-primary"
    });

    // Time Picker
    jQuery('#timepicker').timepicker({
        defaultTIme : false,
        icons: {
            up: 'mdi mdi-chevron-up',
            down: 'mdi mdi-chevron-down'
        }
    });
    jQuery('#timepicker2').timepicker({
        minuteStep : 15,
        icons: {
            up: 'mdi mdi-chevron-up',
            down: 'mdi mdi-chevron-down'
        }
    });
    jQuery('#timepicker3').timepicker({
        minuteStep : 15,
        icons: {
            up: 'mdi mdi-chevron-up',
            down: 'mdi mdi-chevron-down'
        }
    });
    jQuery('#timepicker4').timepicker({
        minuteStep : 15,
        icons: {
            up: 'mdi mdi-chevron-up',
            down: 'mdi mdi-chevron-down'
        }
    });
    jQuery('#timepicker5').timepicker({
        minuteStep : 15,
        icons: {
            up: 'mdi mdi-chevron-up',
            down: 'mdi mdi-chevron-down'
        }
    });
    jQuery('#timepicker6').timepicker({
        minuteStep : 15,
        icons: {
            up: 'mdi mdi-chevron-up',
            down: 'mdi mdi-chevron-down'
        }
    });
    jQuery('#timepicker7').timepicker({
        minuteStep : 15,
        icons: {
            up: 'mdi mdi-chevron-up',
            down: 'mdi mdi-chevron-down'
        }
    });
    //colorpicker start

    $('.colorpicker-default').colorpicker({
        format: 'hex'
    });
    $('.colorpicker-rgba').colorpicker();

    // Date Picker
    jQuery('#datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
    jQuery('#datepicker-inline').datepicker();
    jQuery('#datepicker-multiple-date').datepicker({
        format: "mm/dd/yyyy",
        clearBtn: true,
        multidate: true,
        multidateSeparator: ","
    });
    jQuery('#date-range').datepicker({
        toggleActive: true
    });

    //Date range picker
    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-secondary',
        cancelClass: 'btn-primary'
    });
    $('.input-daterange-timepicker').daterangepicker({
        timePicker: true,
        format: 'MM/DD/YYYY h:mm A',
        timePickerIncrement: 30,
        timePicker12Hour: true,
        timePickerSeconds: false,
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-secondary',
        cancelClass: 'btn-primary'
    });
    $('.input-limit-datepicker').daterangepicker({
        format: 'MM/DD/YYYY',
        minDate: '06/01/2016',
        maxDate: '06/30/2016',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-secondary',
        cancelClass: 'btn-primary',
        dateLimit: {
            days: 6
        }
    });

    $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

    $('#reportrange').daterangepicker({
        format: 'MM/DD/YYYY',
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2016',
        maxDate: '12/31/2016',
        dateLimit: {
            days: 60
        },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-success',
        cancelClass: 'btn-secondary',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Cancel',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
    }, function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    });

    //Bootstrap-MaxLength
    $('input#defaultconfig').maxlength({
        warningClass: "badge badge-success",
        limitReachedClass: "badge badge-danger"
    });

    $('input#thresholdconfig').maxlength({
        threshold: 20,
        warningClass: "badge badge-success",
        limitReachedClass: "badge badge-danger"
    });

    $('input#moreoptions').maxlength({
        alwaysShow: true,
        warningClass: "badge badge-success",
        limitReachedClass: "badge badge-danger"
    });

    $('input#alloptions').maxlength({
        alwaysShow: true,
        warningClass: "badge badge-success",
        limitReachedClass: "badge badge-danger",
        separator: ' out of ',
        preText: 'You typed ',
        postText: ' chars available.',
        validate: true
    });

    $('textarea#textarea').maxlength({
        alwaysShow: true,
        warningClass: "badge badge-success",
        limitReachedClass: "badge badge-danger"
    });

    $('input#placement').maxlength({
        alwaysShow: true,
        placement: 'top-left',
        warningClass: "badge badge-success",
        limitReachedClass: "badge badge-danger"
    });
    $(window).on('load',function(){


        @if (Session::has('message'))

        $('#myModal').modal('show');
        @endif

        @if ($errors->any())

        $('#myModal').modal('show');
        @endif
    });
</script>

<!-- SCRIPTS -->


</body>
</html>