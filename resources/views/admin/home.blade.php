<!DOCTYPE html>
<html>
    
<!-- Mirrored from coderthemes.com/adminto/bs4/light/calendar.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 11:28:57 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="{{ asset('admin/assets/images/favicon.ico') }}">

        <title>Admin Home Page</title>

        <!--calendar css-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,400,700">
        <!-- App css -->
    <link href="{{ asset('admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/assets/css/style.css') }}" rel="stylesheet" type="text/css" />

    <script src="{{ asset('admin/assets/js/modernizr.min.js') }}"></script>
    <style>
        @charset "utf-8";
        /* CSS Document */

        /* ---------- FONTAWESOME ---------- */
        /* ---------- https://fortawesome.github.com/Font-Awesome/ ---------- */
        /* ---------- http://weloveiconfonts.com/ ---------- */

        @import url(http://weloveiconfonts.com/api/?family=fontawesome);

        *[class*="fontawesome-"]:before {
            font-family: 'FontAwesome', sans-serif;
        }

        /* ---------- GENERAL ---------- */

        body {
            background: #f9f9f9;
            color: #0e171c;
            font: 300 100%/1.5em 'Lato', sans-serif;
            margin: 0;
        }

        a {
            text-decoration: none;
        }

        h2 {
            font-size: 2em;
            line-height: 1.25em;
            margin: .25em 0;
        }

        h3 {
            font-size: 1.5em;
            line-height: 1em;
            margin: .33em 0;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
        }


        /* ---------- CALENDAR ---------- */

        .calendar {
            text-align: center;
            width: 100%;
        }

        .calendar header {
            position: relative;
        }

        .calendar h2 {
            text-transform: uppercase;
        }

        .calendar thead {
            font-weight: 600;
            text-transform: uppercase;
        }

        .calendar tbody {
            color: #7c8a95;
        }

        .calendar tbody td:hover {
            border: 2px solid #00addf;
        }

        .calendar td {
            border: 2px solid transparent;
            border-radius: 50%;
            display: inline-block;
            height: 4em;
            line-height: 4em;
            text-align: center;
            width: 4em;
        }

        .calendar .prev-month,
        .calendar .next-month {
            color: #cbd1d2;
        }

        .calendar .prev-month:hover,
        .calendar .next-month:hover {
            border: 2px solid #cbd1d2;
        }

        .current-day {
            background: #00addf;
            color: #f9f9f9;
        }

        .event {
            cursor: pointer;
            position: relative;
        }

        .event:after {
            background: #00addf;
            border-radius: 50%;
            bottom: .5em;
            display: block;
            content: '';
            height: .5em;
            left: 50%;
            margin: -.25em 0 0 -.25em;
            position: absolute;
            width: .5em;
        }

        .event.current-day:after {
            background: #f9f9f9;
        }

        .btn-prev,
        .btn-next {
            border: 2px solid #cbd1d2;
            border-radius: 50%;
            color: #cbd1d2;
            height: 2em;
            font-size: .75em;
            line-height: 2em;
            margin: -1em;
            position: absolute;
            top: 50%;
            width: 2em;
        }

        .btn-prev:hover,
        .btn-next:hover {
            background: #cbd1d2;
            color: #f9f9f9;
        }

        .btn-prev {
            left: 6em;
        }

        .btn-next {
            right: 6em;
        }
    </style>
    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            @include('admin.template.topMenu')
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                @include('admin.template.leftMenu')

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="calendar">
                                    <header>
                                        <h2>Ramadan 2018</h2>
                                        <h2>{{ date("Y / m / d",time()) }}</h2>
                                    </header>
                                    <table>
                                        <thead>
                                        <tr>
                                            <td>Mo</td>
                                            <td>Tu</td>
                                            <td>We</td>
                                            <td>Th</td>
                                            <td>Fr</td>
                                            <td>Sa</td>
                                            <td>Su</td>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><a href="{{ isset($day[0]->day)?  url('day/'.$day[0]->id.'/edit') : '' }}">{{ isset($day[0]->day)? $day[0]->day : '' }}</a></td>
                                            <td><a href="{{ isset($day[1]->day)?  url('day/'.$day[1]->id.'/edit') : '' }}">{{ isset($day[1]->day)? $day[1]->day : '' }}</a></td>
                                            <td><a href="{{ isset($day[2]->day)?  url('day/'.$day[2]->id.'/edit') : '' }}">{{ isset($day[2]->day)? $day[2]->day : '' }}</a></td>
                                            <td><a href="{{ isset($day[3]->day)?  url('day/'.$day[3]->id.'/edit') : '' }}">{{ isset($day[3]->day)? $day[3]->day : '' }}</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="{{ isset($day[4]->day)?  url('day/'.$day[4]->id.'/edit') : '' }}">{{ isset($day[4]->day)? $day[4]->day : '' }}</a></td>
                                            <td><a href="{{ isset($day[5]->day)?  url('day/'.$day[5]->id.'/edit') : '' }}">{{ isset($day[3]->day)? $day[5]->day : '' }}</a></td>
                                            <td><a href="{{ isset($day[6]->day)?  url('day/'.$day[6]->id.'/edit') : '' }}">{{ isset($day[3]->day)? $day[6]->day : '' }}</a></td>
                                            <td><a href="{{ isset($day[7]->day)?  url('day/'.$day[7]->id.'/edit') : '' }}">{{ isset($day[3]->day)? $day[3]->day : '' }}</a></td>
                                            <td><a href="{{ isset($day[8]->day)?  url('day/'.$day[8]->id.'/edit') : '' }}">{{ isset($day[3]->day)? $day[3]->day : '' }}</a></td>
                                            <td><a href="{{ isset($day[9]->day)?  url('day/'.$day[9]->id.'/edit') : '' }}">{{ isset($day[3]->day)? $day[3]->day : '' }}</a></td>
                                            <td><a href="{{ isset($day[10]->day)?  url('day/'.$day[10]->id.'/edit') : '' }}">{{ isset($day[3]->day)? $day[3]->day : '' }}</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="{{ url('day/'.$day[11]->id.'/edit') }}">{{ $day[11]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[12]->id.'/edit') }}">{{ $day[12]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[13]->id.'/edit') }}">{{ $day[13]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[14]->id.'/edit') }}">{{ $day[14]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[15]->id.'/edit') }}">{{ $day[15]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[16]->id.'/edit') }}">{{ $day[16]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[17]->id.'/edit') }}">{{ $day[17]->day }}</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="{{ url('day/'.$day[18]->id.'/edit') }}">{{ $day[18]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[19]->id.'/edit') }}">{{ $day[19]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[20]->id.'/edit') }}">{{ $day[20]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[21]->id.'/edit') }}">{{ $day[21]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[22]->id.'/edit') }}">{{ $day[22]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[23]->id.'/edit') }}">{{ $day[23]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[24]->id.'/edit') }}">{{ $day[24]->day }}</a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="{{ url('day/'.$day[25]->id.'/edit') }}">{{ $day[25]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[26]->id.'/edit') }}">{{ $day[26]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[27]->id.'/edit') }}">{{ $day[27]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[28]->id.'/edit') }}">{{ $day[28]->day }}</a></td>
                                            <td><a href="{{ url('day/'.$day[29]->id.'/edit') }}">{{ $day[29]->day }}</a></td><td></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    </div>
                            </div>
                            <!-- end col-12 -->
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 - 2018 © Adminto. Coderthemes.com
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="{{ asset('admin/assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('admin/assets/js/popper.min.js') }}"></script>
        <script src="{{ asset('admin/assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('admin/assets/js/detect.js') }}"></script>
        <script src="{{ asset('admin/assets/js/fastclick.js') }}"></script>
        <script src="{{ asset('admin/assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('admin/assets/js/waves.js') }}"></script>
        <script src="{{ asset('admin/assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('admin/assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('admin/assets/js/jquery.scrollTo.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('admin/assets/js/jquery.core.js') }}"></script>
        <script src="{{ asset('admin/assets/js/jquery.app.js') }}"></script>

        <!-- Jquery-Ui -->
        <script src="{{ asset('admin/assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

        <!-- SCRIPTS -->


    </body>
</html>