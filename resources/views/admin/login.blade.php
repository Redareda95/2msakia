<!DOCTYPE html>
<html>
    
<!-- Mirrored from coderthemes.com/adminto/bs4/light/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 11:28:59 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="{{ asset('admin/assets/images/favicon.ico') }}">

        <title>Adminto - Responsive Admin Dashboard Template</title>

        <!-- App css -->
        <link href="{{ asset('admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('admin/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('admin/assets/css/style.css') }}" rel="stylesheet" type="text/css" />

        <script src="{{ asset('admin/assets/js/modernizr.min.js') }}"></script>

    </head>

    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="text-center">
                <a href="{{ url('/') }}" class="logo"><span>Welcome Back <span>Admin</span></span></a>
                <h5 class="text-muted m-t-0 font-600">Please Login To Continue</h5>
            </div>
        	<div class="m-t-40 card-box">
                <div class="text-center">
                    <h4 class="text-uppercase font-bold m-b-0">Sign In</h4>
                </div>
                <div class="p-20">
                    <form class="form-horizontal m-t-20" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            </div>
                        </div>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                  <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            </div>
                        </div>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                  <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <div class="form-group text-center m-t-30">
                            <div class="col-xs-12">
                                <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- end card-box-->

            <div class="row">
                <div class="col-sm-12 text-center">
                    <p class="text-muted">Don't have an account? <a href="page-register.html" class="text-primary m-l-5"><b>Sign Up</b></a></p>
                </div>
            </div>
            
        </div>
        <!-- end wrapper page -->



        <!-- jQuery  -->
        <script src="{{ asset('admin/assets/js/modernizr.min.js') }}"></script>
        <script src="{{ asset('admin/asset/js/popper.min.js') }}"></script>
        <script src="{{ asset('admin/asset/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('admin/asset/js/detect.js') }}"></script>
        <script src="{{ asset('admin/asset/js/fastclick.js') }}"></script>
        <script src="{{ asset('admin/asset/js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('admin/asset/js/waves.js') }}assets/js/"></script>
        <script src="{{ asset('admin/asset/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('admin/asset/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('admin/asset/js/jquery.scrollTo.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('admin/asset/js/jquery.core.js') }}"></script>
        <script src="{{ asset('admin/asset/js/jquery.app.js') }}"></script>
	
	</body>

<!-- Mirrored from coderthemes.com/adminto/bs4/light/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 11:28:59 GMT -->
</html>