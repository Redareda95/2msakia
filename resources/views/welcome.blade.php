<!DOCTYPE html>
<!--[if lt IE 8 ]><html class="no-js ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 8)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>

   <!--- Basic Page Needs
   ================================================== -->
   <meta charset="utf-8">
    <title>الامساكيه</title>
    <meta name="description" content="Ramadan Prayer Timings 2018">
    <meta name="author" content="GTSAW">
    <link rel="shortcut icon" href="images/16.png">   <!-- Mobile Specific Metas
   ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
   <link href="https://fonts.googleapis.com/css?family=Amiri" rel="stylesheet">
<!-- Bootstrap core CSS -->
        <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/layout.css">
   <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="favicon.png" >
</head>

<body>

   <header >

     <nav id="nav-wrap">

     
    <div class="header-left clearfix">

                            <!-- logo -->
                            <div class="logo smooth-scroll ">
                                <a href="/"><img id="logo" src="/images/500.png"></a>
                            </div>

                            

                        </div>
    
      </nav> <!-- end #nav-wrap -->


 <div class="video-background hidden-xs hidden-sm">
    <div class="video-foreground">
     <video class="video-bg" autoplay muted loop>
        <source src="18.mp4" type="video/mp4" />
        
    </video>

    </div>
  </div>

      <div class="container banner">
        
        <section>
<h1 >رمضان بيجمعنا</h1>
<p></p>
    <br> 
    <br>
    <br>
 <div class="mybutton ">
  <a id="link" class="threedee" href="/calender">امساكية رمضان  </a>
</div>

</section>
<!--

 <div class="container">
    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
        <button class="btn-success btn-lg">coming soon</button>
    </div>
    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
        <button class="btn-success btn-lg  ">coming soon</button>
    </div>
    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
        <button class="btn-success btn-lg">coming soon</button>
    </div>
 </div>
-->
           <div class="clearfix"></div>
            <ul class="social">
               <li><a href="https://www.facebook.com/GTSAW.NET/"  target="_blank"><i class="fa fa-facebook"></i></a></li>
               <li><a href="https://twitter.com/GTSAW1" target="_blank"><i class="fa fa-twitter"></i></a></li>
               <li><a href="https://mail.google.com/mail/u/4/#inbox"  target="_blank"><i class="fa fa-google-plus"></i></a></li>
               <li><a href="https://www.linkedin.com/in/gtsaw-software-solution-b59007162/"  target="_blank"><i class="fa fa-linkedin"></i></a></li> 
            </ul>
         </div>
   </header> <!-- Header End -->
   <!-- Java Script
   ================================================== -->
   <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
   

   <script src="/js/waypoints.js"></script>
   <script src="/js/init.js"></script>

</body>

</html>