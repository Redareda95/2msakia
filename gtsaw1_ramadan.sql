-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 26, 2018 at 09:06 AM
-- Server version: 5.6.39-83.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gtsaw1_ramadan`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_04_25_085120_create_months_table', 1),
(4, '2018_04_25_085136_create_sliders_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `months`
--

CREATE TABLE `months` (
  `id` int(10) UNSIGNED NOT NULL,
  `melady_day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `day` int(11) NOT NULL,
  `emsak` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fager` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sherouk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dohr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maghreb` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `esha` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `day_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `months`
--

INSERT INTO `months` (`id`, `melady_day`, `day`, `emsak`, `fager`, `sherouk`, `dohr`, `asr`, `maghreb`, `esha`, `created_at`, `updated_at`, `day_name`) VALUES
(1, '05/17/2018', 1, '3:26', '3:26', '5:00', '11:51', '3:28', '6:43', '8:13', '2018-04-25 11:46:25', '2018-04-25 11:46:25', 'الخميس'),
(2, '05/18/2018', 2, '3:26', '3:26', '5:00', '11:51', '3:28', '6:43', '8:13', '2018-04-25 11:51:44', '2018-04-25 11:51:44', 'الجمعه'),
(3, '05/19/2018', 3, '3:25', '3:25', '4:59', '11:51', '3:28', '6:44', '8:14', '2018-04-25 11:54:40', '2018-04-25 11:54:40', 'السبت'),
(4, '05/20/2018', 4, '3:24', '3:24', '4:59', '11:52', '3:28', '6:45', '8:15', '2018-04-25 09:46:25', '2018-04-25 09:46:25', 'الاحد'),
(5, '05/21/2018', 5, '3:23', '3:23', '4:58', '11:52', '3:28', '6:45', '8:15', '2018-04-25 09:51:44', '2018-04-25 09:51:44', 'الاثنين'),
(6, '05/22/2018', 6, '3:23', '3:23', '4:58', '11:52', '3:28', '6:46', '8:16', '2018-04-25 09:54:40', '2018-04-25 09:54:40', 'الثلاثاء'),
(7, '05/23/2018', 7, '3:22', '3:22', '4:57', '11:52', '3:28', '6:46', '8:16', '2018-04-25 09:46:25', '2018-04-25 09:46:25', 'الاربعاء'),
(8, '05/24/2018', 8, '3:21', '3:21', '4:57', '11:52', '3:28', '6:47', '8:17', '2018-04-25 09:51:44', '2018-04-25 09:51:44', 'الخميس'),
(9, '05/25/2018', 9, '3:20', '3:20', '4:57', '11:52', '3:28', '6:48', '8:18', '2018-04-25 09:54:40', '2018-04-25 09:54:40', 'الجمعه'),
(10, '05/26/2018', 10, '3:20', '3:20', '4:56', '11:52', '3:28', '6:48', '8:18', '2018-04-25 09:46:25', '2018-04-25 09:46:25', 'السبت'),
(11, '05/27/2018', 11, '3:19', '3:19', '4:56', '11:52', '3:29', '6:49', '8:19', '2018-04-25 09:51:44', '2018-04-25 09:51:44', 'الاحد'),
(12, '05/28/2018', 12, '3:19', '3:19', '4:55', '11:52', '3:29', '6:49', '8:19', '2018-04-25 09:54:40', '2018-04-25 09:54:40', 'الاثنين'),
(13, '05/29/2018', 13, '3:18', '3:18', '4:55', '11:52', '3:29', '6:50', '8:20', '2018-04-25 09:46:25', '2018-04-25 09:46:25', 'الثلاثاء'),
(14, '05/30/2018', 14, '3:18', '3:18', '4:55', '11:53', '3:29', '6:50', '8:20', '2018-04-25 09:51:44', '2018-04-25 09:51:44', 'الاربعاء'),
(15, '05/31/2018', 15, '3:17', '3:17', '4:55', '11:53', '3:29', '6:51', '8:21', '2018-04-25 09:54:40', '2018-04-25 09:54:40', 'الخميس'),
(16, '06/01/2018', 16, '3:17', '3:17', '4:54', '11:53', '3:29', '6:52', '8:22', '2018-04-25 09:46:25', '2018-04-25 09:46:25', 'الجمعه'),
(17, '06/02/2018', 17, '3:16', '3:16', '4:54', '11:53', '3:29', '6:52', '8:22', '2018-04-25 09:51:44', '2018-04-25 09:51:44', 'السبت'),
(18, '06/03/2018', 18, '3:16', '3:16', '4:54', '11:53', '3:29', '6:53', '8:23', '2018-04-25 09:54:40', '2018-04-25 09:54:40', 'الاحد'),
(19, '06/04/2018', 19, '3:16', '3:16', '4:54', '11:53', '3:29', '6:53', '8:23', '2018-04-25 09:46:25', '2018-04-25 09:46:25', 'الاثنين'),
(20, '06/05/2018', 20, '3:15', '3:15', '4:54', '11:53', '3:29', '6:54', '8:24', '2018-04-25 09:51:44', '2018-04-25 09:51:44', 'الثلاثاء'),
(21, '06/06/2018', 21, '3:15', '3:15', '4:53', '11:54', '3:30', '6:54', '8:24', '2018-04-25 09:54:40', '2018-04-25 09:54:40', 'الاربعاء'),
(22, '07/07/2018', 22, '3:15', '3:15', '4:53', '11:54', '3:30', '6:54', '8:24', '2018-04-25 09:46:25', '2018-04-25 09:46:25', 'الخميس'),
(23, '06/08/2018', 23, '3:14', '3:14', '4:53', '11:54', '3:30', '6:55', '8:25', '2018-04-25 09:51:44', '0000-00-00 00:00:00', 'الجمعه'),
(24, '06/08/2018', 24, '3:14', '3:14', '4:53', '11:54', '3:30', '6:55', '8:25', '2018-04-25 09:54:40', '2018-04-25 09:54:40', 'السبت'),
(25, '06/10/2018', 25, '3:14', '3:14', '4:53', '11:54', '3:30', '6:56', '8:26', '2018-04-25 09:46:25', '2018-04-25 09:46:25', 'الاحد'),
(26, '06/11/2018', 26, '3:14', '3:14', '4:53', '11:55', '3:30', '6:56', '8:26', '2018-04-25 09:51:44', '2018-04-25 09:51:44', 'الاثنين'),
(27, '06/12/2018', 27, '3:14', '3:14', '4:53', '11:55', '3:30', '6:57', '8:27', '2018-04-25 09:54:40', '2018-04-25 09:54:40', 'الثلاثاء'),
(28, '06/13/2018', 28, '3:14', '3:14', '4:53', '11:55', '3:31', '6:57', '8:27', '2018-04-25 09:46:25', '2018-04-25 09:46:25', 'الاربعاء'),
(29, '06/14/2018', 29, '3:14', '3:14', '4:53', '11:55', '3:31', '6:57', '8:27', '2018-04-25 09:51:44', '2018-04-25 09:51:44', 'الخميس'),
(30, '06/15/2018', 30, '3:14', '3:14', '4:53', '11:55', '3:31', '6:58', '8:28', '2018-04-25 09:54:40', '2018-04-25 09:54:40', 'الجمعه');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission` enum('superAdmin','admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'admin',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `phone_number`, `password`, `image`, `permission`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'mohamed', 'admin@gmail.com', 'mohamed', '01128142458', '$2y$10$u9Ctk9lrMBXdEmlsiNbfY.29SxQ7vfpymGGNkELzloYJYAA/jo7EW', 'mohamed.png', 'admin', 'RlD593hf2a4DAh58YSdvWIKwnnaTy2BYX210MwRVe2gD0rOS7R9XDxclnSR0', '2018-04-24 22:00:00', '2018-04-24 22:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `months`
--
ALTER TABLE `months`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `day` (`day`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_phone_number_unique` (`phone_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `months`
--
ALTER TABLE `months`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
