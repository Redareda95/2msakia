<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Month;

class PreviewController extends Controller
{
    public function index()
    {
    	$monthes = Month::orderBy('day', 'asc')->get();
    	return view('mainCalender', compact('monthes'));
    }

    public function show($id)
    {
    	$month = Month::find($id);

    	return view('day', compact('month'));
    }
}
