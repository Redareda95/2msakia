<?php

namespace App\Http\Controllers;

use App\Month;
use Illuminate\Http\Request;

class MonthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $day = Month::get();
        return view('admin.home',compact('day'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Month::get()->count() < 30)
        {
            $add=1;
            return view('admin.day',compact('add'));
        }
        else
        {
            return redirect()->back()->with('message','error : ramadan day is completed => 30 day');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(Month::get()->count() < 30)
        {
            $data = $request->all();
            Month::create($data);
            return redirect()->back()->with('message','successful adding ramadan day');
        }
        else
        {
            return redirect()->back()->with('message','error : ramadan day is completed => 30 day');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Month  $month
     * @return \Illuminate\Http\Response
     */
    public function show(Month $month)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Month  $month
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Month::find($id);
        return view('admin.day',compact('edit'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Month  $month
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         Month::find($id)->update($request->all());
         return redirect()->back()->with('message','successfully updating this day');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Month  $month
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Month::find($id)->delete();
        return redirect()->back()->with('message','successfully deleting this day');
    }
}
