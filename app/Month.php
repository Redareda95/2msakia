<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Month extends Model
{
    protected $fillable = ['day', 'melady_day','sherouk' , 'emsak', 'fager', 'dohr', 'asr', 'maghreb', 'esha'];
}